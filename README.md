# Facatino

This repo contains several FASTApi microservices and a flask web site.

## Services
* coinlist - Provides a backend to maintain coin information such as ticker and RPC info for the coind.
  * [Documentation](https://facatino.github.io/facatino/wallet)
* wallet - An account ledger for all coins to manage balances, withdrawls and deposits.
  * [Documentation](https://facatino.github.io/facatino/coinlist)
* discord-bot - A sample implementation to grant a discord role when users deposit crypto to a provided address.
* db - Postgresql container

## Install
First things first, you need to clone the repo before you start the setup configurations


* Clone the repo
```
git clone https://github.com/facatino/facatino.git
```

## Setup
The configuration of facatino requires you to define passwords in the postgresql provisioning script as well as create `.env` files for the wallet and coinlist services.

* Postgresql

  The psql docker instance is provisioned with credentials for the services. You must edit the config example provided.
  ```
  cp init-sql/init-user-db.sh.sample init-sql/init-user-db.sh
  ```
  * Edit these fields
    * COINLIST_USER_PASSWORD
    * WALLET_USER_PASSWORD
    * SITE_USER_PASSWORD


* Wallet Service
  You must modify the `.env` files used by the services, use the passwords configured in the Postgresql step.
  ```
  #wallet/app/db/.env
  SQLALCHEMY_DATABASE_URI=postgresql://wallet:<PASSWORD>@db/wallet
  ```
 * Coinlist Service
  You must modify the `.env` files used by the services, use the passwords configured in the Postgresql step.
   ```
   # coinlist/app/db/.env
   SQLALCHEMY_DATABASE_URI=postgresql://coinlist:<PASSWORD>@db/coinlist
   ```  

## Startup

* Build docker containers:
```
docker-compose build
```
* Start services
```
docker-compose up
```
  
  


