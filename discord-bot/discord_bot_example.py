import sys
from logging import exception
import requests
import os
import discord
import random
from dotenv import load_dotenv
import urllib.parse
import requests
from discord.ext import commands

# Fetch the Bot Token for .env file
load_dotenv()
TOKEN = os.getenv("DISCORD_BOT_TOKEN")

# Configure the desciption of the bot
description = """FacatinoBot Example"""

# Grant intents/permissions.
# Intents function as roles in Discord
intents = discord.Intents.default()
intents.members = True

# Start the bot object, specifying your command_prefix.
# The command prefix is the character used at the start of each command.
bot = commands.Bot(command_prefix="?", description=description, intents=intents)

# Declare a handful of global configuration options.
#
# The CAPS are meant to not change throughout processing
# The lowercase are intended to be updated as the bot runs.

# coin_info is used as a stateful location for the list of coins supported
coin_info = {}

# For `gamble` example command, if set to 0 there is no MAX_BET
MAX_BET = 100

# for `contest` example, this is the required deposit
CONTEST_MIN_COINS = 2000
# for `contest` example, this is the coin you require deposit in
COINTEST_COIN_ID = 8
# for `contest` example, this is the discord role granted when `CONTEST_MIN_COINS` is reached
DISCORD_GRANT_ROLE = "Contest"


@bot.event
async def on_ready():
    """
    Connect to Discord and provide invite URL

    Once connected, the bot will run `on_ready`
    This states the bot's username and gives a URL
    to be used to invite the bot to a discord.
    """
    print(f"{bot.user.name} has connected to Discord!")
    print(
        f"You can now invite the bot to a server using the following link: https://discordapp.com/oauth2/authorize?client_id={bot.user.id}&scope=bot"
    )

    # Run any post-startup commands

    # Running `get_coin_info` will generate our list of supported coins
    await get_coin_info()


@bot.command(name="cat")
async def cat(ctx):
    """Display a cat from cataas.com

    This is a super basic example on using an API to
    fetch a response.

    The `cat` image will add a random number to the
    end. This is because the URL never changes and
    discord will cache it.
    """
    response = "https://cataas.com/cat?width=250&id=" + str(random.randrange(0, 232323))

    await ctx.send(response)


# Listcoins


async def get_coin_info():
    """Fetch all the coin info so we can support multiple blockchains

    This is a dependency of the `listcoins` command, and others.
    It is used to reach out to the coinlist API and gather `coin_id`
    and the `name` of each coin.  These are used when a user's command
    will specify the `name` of a coin.
    """
    try:

        response = requests.get("http://localhost:5050/coins")
    except Exception as e:
        print(f"Unable to connect to coinlist API: {e}")

    for coin in response.json():
        name = str(coin["name"]).lower()
        coin_id = coin["id"]
        coin_info[name] = coin_id


@bot.command(name="listcoins")
async def listcoins(ctx):
    """List all the coins available

    This is the first example of using the facatino coinlist
    API.  It will create a list of coins supported by the
    endpoint.  It is used as an catalog to lookup the `coin_id`
    of a coin requested by name.
    """
    coin_list = ""

    coin_list = dict((k.capitalize(), v) for k, v in coin_info.items())
    formated_list = ", ".join(list(coin_list.keys()))
    response = f"Supported coins are: {formated_list}"

    await ctx.send(response)


@bot.command(name="generate")
async def generate(ctx, *, coin_name: str = None):
    """Generate a new address for a Discord user

    This will create an account for the discord user
    on the preferred COIN_ID daemon.

    """
    # Did they ask for real coin?
    try:
        coin_id = coin_info[coin_name]
    except:
        response = f"{ctx.author.mention}, that is an invalid coin, use `!listcoins` to see valid coin names"
        await ctx.send(response)
        return

    # Create a new account for `coin_id` and `address_label`,
    # which is the discord userID
    wallet = requests.post(
        "http://localhost:5040/account/add",
        params={
            "coin_id": coin_id,
            "account_label": str(ctx.author.id),
        },
    )

    # If we successfully add a user, get the users account address
    if wallet.status_code == 200:
        address = requests.get(
            "http://localhost:5040/account/address",
            params={
                "coin_id": coin_id,
                "account_label": str(ctx.author.id),
            },
        ).json()["address"]
        response = f"Hello {ctx.author.mention}! Your deposit address is {address}"
    else:
        response = f"Sorry {ctx.author.mention}, Failed to add your account"

    await ctx.send(response)


@bot.command(name="validate")
async def validate(ctx, coin_name: str = None, address: str = None):
    """Validate a blockchain address

    This connects to the wallet API to ask the
    blockchain daemon if the address is valid
    """

    # Did they ask for real coin?
    try:
        coin_id = coin_info[coin_name]
    except:
        response = f"{ctx.author.mention}, that is an invalid coin, use `!validate coin_name address` to see validate an address"
        await ctx.send(response)
        return

    req = requests.get(
        "http://localhost:5040/validate",
        params={"coin_id": coin_id, "address": address},
    )

    if req.json()["isvalid"]:
        response = f"{ctx.author.mention} the address you provided, {address}, is a valid {coin_name} address"
    else:
        response = (
            f"{ctx.author.mention} sorry, {address}, is not a valid blockchain address"
        )

    await ctx.send(response)


@bot.command(name="send")
async def send(ctx, coin_name: str = None, address: str = None, amount: float = None):
    """Send funds to an address on the blockchain

    This is a verbose example of doing multiple verification
    checks before completeting a function. Ensuring the coin
    exists, the destination address is valid, the sender has
    the balance required and of course, enusring they ask for
    a real coin.
    """
    # Confirm the user provided all required arguments
    for arg in [coin_name, address, amount]:
        if arg == None:
            response = f"{ctx.author.mention}, you must provide the format, `!send coin_name address amount`"
            await ctx.send(response)
            return

    # Did they ask for real coin?
    try:
        coin_id = coin_info[coin_name]
    except:
        response = f"{ctx.author.mention}, that is an invalid coin, use `!listcoins` to see valid coin names"
        await ctx.send(response)
        return

    # Validate it's a real address
    req = requests.get(
        "http://localhost:5040/validate",
        params={"coin_id": coin_id, "address": address},
    )

    if not req.json()["isvalid"]:
        response = f"{ctx.author.mention} sorry, {address}, is not a valid blockchain address for {coin_name}"
        await ctx.send(response)
        return

    if req.json()["ismine"]:
        response = f"{ctx.author.mention} sorry, you cannot send to another address on the same bot"
        await ctx.send(response)
        return

    # Is that a real number they're asking for?
    try:
        amount = float(amount)
    except:
        response = f"{ctx.author.mention}, that is not a valid amount"
        await ctx.send(response)
        return

    # Confirm they have the balance they are requesting
    balance = requests.get(
        "http://localhost:5040/account/balance",
        params={"coin_id": coin_id, "account_label": str(ctx.author.id)},
    )

    if float(balance.text) < amount:
        response = f"{ctx.author.mention} You current balance is too low for this action, you only have {balance.text}"
        await ctx.send(response)
        return

    # Confirm they are not sending to themselves
    messager_address = requests.get(
        "http://localhost:5040/account/address",
        params={
            "coin_id": coin_id,
            "account_label": str(ctx.author.id),
        },
    ).json()["address"]

    if messager_address == address:
        response = f"{ctx.author.mention} You cannot send to yourself"
        await ctx.send(response)
        return

    # Send the actual balance
    send_funds = requests.post(
        "http://localhost:5040/account/send",
        params={
            "coin_id": coin_id,
            "account_label": str(ctx.author.id),
            "address": address,
            "quantity": amount,
        },
    )
    response = f"{ctx.author.mention} survey says: {send_funds.text}"

    await ctx.send(response)


@bot.command(name="getbalance")
async def getbalance(ctx, coin_name: str = None):
    """Get a user balance

    This connects to the wallet API to access the
    balance directly from the coin daemon RPC
    """
    # Confirm the user provided all required arguments
    for arg in [coin_name]:
        if arg == None:
            response = f"{ctx.author.mention}, you must provide the format, `!getbalance coin_name`"
            await ctx.send(response)
            return

    # Did they ask for real coin?
    try:
        coin_id = coin_info[coin_name]
    except:
        response = f"{ctx.author.mention}, that is an invalid coin, use `!listcoins` to see valid coin names"
        await ctx.send(response)
        return

    balance = requests.get(
        "http://localhost:5040/account/balance",
        params={"coin_id": coin_id, "account_label": str(ctx.author.id)},
    )
    response = f"{ctx.author.mention} You current balance is {balance.text}"

    await ctx.send(response)


@bot.command(name="gamble")
async def gamble(ctx, coin_name: str = None, amount: float = None):
    """
    Gamble funds available
    """
    await ctx.message.delete()
    # Confirm the user provided all required arguments
    for arg in [coin_name, amount]:
        if arg == None:
            response = f"{ctx.author.mention}, you must provide the format, `!gamble coin_name amount`"
            await ctx.send(response)
            return

    # Did they ask for real coin?
    try:
        coin_id = coin_info[coin_name]
    except:
        response = f"{ctx.author.mention}, that is an invalid coin, use `!listcoins` to see valid coin names"
        await ctx.send(response)
        return

    # Did they ask for whatcoin? That's all we support right now.
    if coin_name.lower() != "whatcoin":
        response = (
            f"{ctx.author.mention}, I'm sorry, only Whatcoin is funded at the moment"
        )
        await ctx.send(response)
        return

    # Is that a real number they're asking for?
    try:
        amount = float(amount)
    except:
        response = f"{ctx.author.mention}, that is not a valid amount"
        await ctx.send(response)
        return

    # Are we capping it? if so, lets do it.
    if MAX_BET > 0 and amount > MAX_BET:
        response = f"{ctx.author.mention}, max bet is 100"
        await ctx.send(response)
        return

    # Confirm they have the balance they are requesting
    balance = requests.get(
        "http://localhost:5040/account/balance",
        params={"coin_id": coin_id, "account_label": str(ctx.author.id)},
    )

    if float(balance.text) < amount:
        response = f"{ctx.author.mention} You current balance is too low for this action, you only have {balance.text}"
        await ctx.send(response)
        return

    payout_factor = amount * 50
    payout = 0
    # Save the wildcard number as spinned_result
    spinned_result = random.randint(0, 100)

    if spinned_result in range(0, 40):  # 40% Chance
        payout = -1  # str(float('0.01') * payout_factor)
    elif spinned_result in range(40, 56):  # 16% Chance
        payout = 0  # str(float('0.02') * payout_factor)
    elif spinned_result in range(56, 70):  # 14% Chance
        payout = float("0.03") * payout_factor
    elif spinned_result in range(70, 82):  # 12% Chance
        payout = float("0.04") * payout_factor
    elif spinned_result in range(82, 89):  # 7% Chance
        payout = float("0.05") * payout_factor
    elif spinned_result in range(89, 95):  # 6% Chance
        payout = float("0.06") * payout_factor
    elif spinned_result in range(95, 99):  # 4% Chance
        payout = float("0.07") * payout_factor
    elif spinned_result in range(99, 100):  # 1% Chance
        payout = float("0.08") * payout_factor

    if payout > 0:
        response = f"{ctx.author.mention} You win {payout}"
        await ctx.send(response)

        headers = {"Accept": "application/json"}

        r = requests.post(
            "http://localhost:5040/account/move",
            params={
                "coin_id": coin_id,
                "account_label_from": "system",
                "account_label_to": str(ctx.author.id),
                "quantity": payout,
            },
            headers=headers,
        )

    elif payout == 0:
        response = f"{ctx.author.mention} You lose {amount}"
        await ctx.send(response)

        headers = {"Accept": "application/json"}

        r = requests.post(
            "http://localhost:5040/account/move",
            params={
                "coin_id": coin_id,
                "account_label_from": str(ctx.author.id),
                "account_label_to": "system",
                "quantity": amount,
            },
            headers=headers,
        )
    else:
        response = f"{ctx.author.mention} You lose {amount * 1.75}"
        await ctx.send(response)

        headers = {"Accept": "application/json"}

        r = requests.post(
            "http://localhost:5040/account/move",
            params={
                "coin_id": coin_id,
                "account_label_from": str(ctx.author.id),
                "account_label_to": "system",
                "quantity": amount * 1.75,
            },
            headers=headers,
        )


bot.run(TOKEN)
