from fastapi.testclient import TestClient
"""
    This is the test harness for the coinlist service

    Here we can validate adding blockchain coin 
    configurations, fetching and deleting them.
"""
from main import app

client = TestClient(app)


def test_invalid_url():
    """
    Confirm we receive a 404 on an invalid url
    """
    response = client.get("/bad_url")
    assert response.status_code == 404


def test_create_coin():
    """
    Validate we can create a coin.
    """
    headers = {"Content-Type": "application/json", "Accept": "application/json"}
    response = client.post(
        "/coins",
        headers=headers,
        json={
            "name": "string",
            "symbol": "string",
            "enable": 1,
            "created": "2019-08-24",
            "rpc_user": "string",
            "rpc_passwd": "string",
            "rpc_host": "string",
            "rpc_port": 0,
        },
    )
    assert response.status_code == 200
    assert response.json() == {
        "id": 1,
        "name": "string",
        "symbol": "string",
        "enable": 1,
        "created": "2019-08-24",
        "rpc_user": "string",
        "rpc_passwd": "string",
        "rpc_host": "string",
        "rpc_port": 0,
    }


def test_create_coin_bad():
    """
    Testing an invalid format, to confirm its converting
    invalid formats to correct ones
    """

    headers = {"Content-Type": "application/json", "Accept": "application/json"}
    response = client.post(
        "/coins",
        headers=headers,
        json={
            "name": 1,
            "symbol": "string",
            "enable": 1,
            "created": 0,
            "rpc_user": 0,
            "rpc_passwd": "string",
            "rpc_host": "string",
            "rpc_port": 0,
        },
    )
    assert response.status_code == 200
    assert response.json() == {
        "id": 2,
        "name": "1",
        "symbol": "string",
        "enable": 1,
        "created": "1970-01-01",
        "rpc_user": "0",
        "rpc_passwd": "string",
        "rpc_host": "string",
        "rpc_port": 0,
    }


def test_get_one_coin():
    """
    Validate we can get a coin.
    """

    response = client.get("/coins/1")
    assert response.status_code == 200
    assert response.json() == {
        "id": 1,
        "name": "string",
        "symbol": "string",
        "enable": 1,
        "created": "2019-08-24",
        "rpc_user": "string",
        "rpc_passwd": "string",
        "rpc_host": "string",
        "rpc_port": 0,
    }


def test_get_all_coins():
    """
    Ensure we can fetch all coins
    """

    response = client.get("/coins")
    assert response.status_code == 200
    print(response.json())
    assert response.json() == [
        {
            "id": 1,
            "name": "string",
            "symbol": "string",
            "enable": 1,
            "created": "2019-08-24",
            "rpc_user": "string",
            "rpc_passwd": "string",
            "rpc_host": "string",
            "rpc_port": 0,
        },
        {
            "id": 2,
            "name": "1",
            "symbol": "string",
            "enable": 1,
            "created": "1970-01-01",
            "rpc_user": "0",
            "rpc_passwd": "string",
            "rpc_host": "string",
            "rpc_port": 0,
        },
    ]


def test_update_coin():
    """
    Test updating a coin
    """

    headers = {"Content-Type": "application/json", "Accept": "application/json"}
    response = client.put(
        "/coins/1",
        headers=headers,
        json={
            "name": 1,
            "symbol": "test",
            "enable": 1,
            "created": 0,
            "rpc_user": 0,
            "rpc_passwd": "string",
            "rpc_host": "string",
            "rpc_port": 0,
        },
    )
    assert response.status_code == 200
    assert response.json() == {
        "id": 1,
        "name": "1",
        "symbol": "test",
        "enable": 1,
        "created": "1970-01-01",
        "rpc_user": "0",
        "rpc_passwd": "string",
        "rpc_host": "string",
        "rpc_port": 0,
    }

def test_delete_coin():
    """
    Test deleting a coin
    """

    headers = {"Content-Type": "application/json", "Accept": "application/json"}
    response = client.delete(
        "/coins/1",
        headers=headers,
        json={
            "name": 1,
            "symbol": "test",
            "enable": 1,
            "created": 0,
            "rpc_user": 0,
            "rpc_passwd": "string",
            "rpc_host": "string",
            "rpc_port": 0,
        },
    )
    assert response.status_code == 200
    assert response.json() == {
        "id": 1,
        "name": "1",
        "symbol": "test",
        "enable": 1,
        "created": "1970-01-01",
        "rpc_user": "0",
        "rpc_passwd": "string",
        "rpc_host": "string",
        "rpc_port": 0,
    }
