from fastapi import FastAPI
from fastapi_crudrouter import DatabasesCRUDRouter

from db.config import database
from db.schemas.coin_schema import Coins
from db.tables.coin_table import coin

description = """
A Cryptocurrency datastore for RPC and general info for a blockchain coin.
"""

app = FastAPI(title="coinlist", description=description, version="0.1.0")


@app.on_event("startup")
async def startup():
    await database.connect()


@app.on_event("shutdown")
async def shutdown():
    await database.disconnect()


router = DatabasesCRUDRouter(schema=Coins, table=coin, database=database)
app.include_router(router)
