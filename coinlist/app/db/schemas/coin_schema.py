from datetime import date
from pydantic import BaseModel


class Coins(BaseModel):
    id: int
    name: str
    symbol: str
    enable: int
    created: date
    rpc_user: str
    rpc_passwd: str
    rpc_host: str
    rpc_port: int

    class Conifg:
        orm_mode = True
