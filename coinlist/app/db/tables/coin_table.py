import sqlalchemy

from db.config import metadata, engine

coin = sqlalchemy.Table(
    "coins",
    metadata,
    sqlalchemy.Column("id", sqlalchemy.Integer, primary_key=True),
    sqlalchemy.Column("name", sqlalchemy.String),
    sqlalchemy.Column("symbol", sqlalchemy.String),
    sqlalchemy.Column("enable", sqlalchemy.Integer),
    sqlalchemy.Column("created", sqlalchemy.Date),
    sqlalchemy.Column("rpc_user", sqlalchemy.String),
    sqlalchemy.Column("rpc_passwd", sqlalchemy.String),
    sqlalchemy.Column("rpc_host", sqlalchemy.String),
    sqlalchemy.Column("rpc_port", sqlalchemy.Integer),
)

metadata.create_all(bind=engine)
