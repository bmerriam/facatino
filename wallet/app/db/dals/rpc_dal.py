import json
import httpx
from fastapi.logger import logger


class RpcDAL:
    """A class to access a blockchain RPC endpoint

    Attributes
    ----------
    coin_id : the coin_id of the RPC daemon
    headers : The json headers for requests

    Methods
    -------
    get_account_balance()
        Get the account balance from the RPC endpoint

    get_account_address()
        Get a blockchain address for an account with an account_label with `getaddressesbyaccount`

    create_new_account()
        Create a new account in the blockchain daemon with `getnewaddress`

    move_funds()
        Move funds from one account label to another account label in an account daemon

    rpc_call()
        Used to connect to an RPC endpoint of a blockchain daemon

    get_coin_info()
        Access the coinlist API to gather details about a coin_id
    """

    def __init__(self, coin_id):
        """
        Parameters
        ----------
        coin_id : str
           the coin_id of the RPC daemon
        headers : str
            The json headers for requests
        """
        self.coin_id = coin_id
        self.headers = {"content-type": "application/json"}

    async def validate_address(self, coin_id: int, address: str):
        """Get the account balance from an RPC endpoint

        Access the blockchain daemon endpoint to get a balance
        for the account_label provided.

        Parameters
        ----------
        coin_id : int
            The id of the blockchain coin on the coinList API
        address : str
            The address to validate on the blockhain
        """
        address = await self.rpc_call(coin_id, "validateaddress", address)
        return address

    async def get_account_balance(self, coin_id: int, account_label: str):
        """Get the account balance from an RPC endpoint

        Access the blockchain daemon endpoint to get a balance
        for the account_label provided.

        Parameters
        ----------
        coin_id : int
            The id of the blockchain coin on the coinList API
        account_label : str
            The account identifier used between the blockchain daemon
            and the database
        """
        balance = await self.rpc_call(coin_id, "getbalance", account_label)
        return balance

    async def get_account_address(self, coin_id: int, account_label: str):
        """Get an account address from the RPC endpoint

        Access the blockchain daemon endpoint to get an account
        address for the account_label provided.

        Parameters
        ----------
        coin_id : int
            The id of the blockchain coin on the coinList API
        account_label : str
            The account identifier used between the blockchain daemon
            and the database
        """

        address = await self.rpc_call(coin_id, "getaddressesbyaccount", account_label)
        return address

    async def create_new_account(self, coin_id: int, account_label: str):
        """Create a new account on a blockchain daemon RPC endpoint

        Create a new account on a blockchain daemon via the RPC
        endpoint for the coin_id provided.

        Parameters
        ----------
        coin_id : int
            The id of the blockchain coin on the coinList API
        account_label : str
            The account identifier used between the blockchain daemon
            and the database
        """

        address = await self.rpc_call(coin_id, "getnewaddress", account_label)
        return address

    async def move_funds(
        self, coin_id, account_label_from: str, account_label_to: str, quantity: float
    ):
        """Move funds from one account to another in a blockchain daemon

        Move a funds on a blockchain daemon from one account to another. This
        only supports one blockchain daemon, for the coin_id provided.

        Parameters
        ----------
        coin_id : int
            The id of the blockchain coin on the coinList API
        account_label : str
            The account identifier used between the blockchain daemon
            and the database
        quantity : float
        """

        params = [account_label_from, account_label_to, quantity]
        move_response = await self.rpc_call(coin_id, "move", params)
        return move_response

    async def send_to_address(
        self, coin_id, account_label: str, address: str, quantity: float
    ):
        """Send funds to an address on the blockhain.

        This is a send to an address on the blockchain. It serves
        as a withdrawl method. The address must be validated prior
        to this step.

        Parameters
        ----------
        coin_id : int
            The id of the blockchain coin on the coinList API
        account_label : str
            The source account identifier used in the transfer
        address : str
            The destination address on the blockchain
        quantity : float
            How many coins to send on the blockchain
        """

        params = [account_label, address, quantity]
        move_response = await self.rpc_call(coin_id, "sendfrom", params)

    async def rpc_call(self, coin_id, method, params):
        """Perform a call to the RPC endpoint

        Generic function to connect to a blockchain daemon RPC
        endpoint. This takes the method, which is the command,
        and the params which act as arguments.

        Parameters
        ----------
        coin_id : int
            The id of the blockchain coin on the coinList API
        method : str
            The method or command to execute against the endpoint.
        params : str
            The arguments when required for a method.
        """

        rpc_info = await self.get_coin_info(self.coin_id)
        logger.info(f"Connecting to {rpc_info['name']}")
        
        if "detail" in rpc_info:
            return "Invalid Coin"

        if not isinstance(params, list):
            params = [params]

        rpc_url = f"http://{rpc_info['rpc_user']}:{rpc_info['rpc_passwd']}@{rpc_info['rpc_host']}:{rpc_info['rpc_port']}"

        rpc_input = {
            "jsonrpc": "2.0",
            "id": "test",
            "method": method,
            "params": params,
        }

        # execute the rpc request
        async with httpx.AsyncClient() as client:
            response = await client.post(
                rpc_url, data=json.dumps(rpc_input), headers=self.headers
            )
        print(response.json())
        return response.json()["result"]

    async def get_coin_info(self, coin_id):
        """Connect to the coinlist API to get details for a blockchain daemon.

        Connect to the coinlist API to get details about a coin,
        such as the RPC conenction details, image and site.

        Parameters
        ----------
        coin_id : int
            The id of the blockchain coin on the coinList API
        """

        async with httpx.AsyncClient() as client:
            response = await client.get(f"http://127.0.0.1:5050/coins/{coin_id}")

        return response.json()
