from db.config import database
from db.tables.wallet_table import account


class AccountDAL:
    """A class to interact with the account model in the DB

    Attributes
    ----------
    database : the database object

    Methods
    -------
    get_account_address()
        Get an account address from the DB by the account_label

    store_new_account()
        Save an account address to the DB by the account_label
    """

    def __init__(self, database: database):
        """
        Parameters
        ----------
        database : class
            The database object
        """
        self.database = database

    async def get_account_address(self, coin_id: int, account_label: str):
        """Get account address from the database

        Using the account_label to query the address for an account
        in the corresponding coin_id that was requested.

        Parameters
        ----------
        coin_id : int
            The id of the blockchain coin on the coinList API
        account_label : str
            The account identifier used between the blockchain daemon
            and the database
        """
        query = "SELECT address FROM account WHERE coin_id = :coin_id AND account_label = :account_label"
        row = await self.database.fetch_one(
            query=query, values={"coin_id": coin_id, "account_label": account_label}
        )

        return row

    async def store_new_account(self, coin_id: int, account_label: str, address: str):
        """Store the account to the database

        Save the account address to the database by the account_label
        for the coin_id provided.

        Parameters
        ----------
        coin_id : int
            The id of the blockchain coin on the coinList API
        account_label : str
            The account identifier used between the blockchain daemon
            and the database
        address : str
            The address of the account on the blockchain daemon
        """
        query = account.insert()
        values = {
            "account_label": account_label,
            "coin_id": coin_id,
            "address": address,
        }
        await database.execute(query=query, values=values)
