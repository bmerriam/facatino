import sqlalchemy

from db.config import metadata, engine

account = sqlalchemy.Table(
    "account",
    metadata,
    sqlalchemy.Column("id", sqlalchemy.Integer, primary_key=True),
    sqlalchemy.Column("account_label", sqlalchemy.String),
    sqlalchemy.Column("coin_id", sqlalchemy.Integer),
    sqlalchemy.Column("address", sqlalchemy.String),
)


metadata.create_all(bind=engine)
