from fastapi import FastAPI
import db.config as config
import routers.account_router as account_router
import routers.validate_router as validate_router

description = """
A Cryptocurrency account ledger to interact with RPC Daemons.
"""

app = FastAPI(title="wallet", description=description, version="0.1.0")


@app.on_event("startup")
async def startup():
    await config.database.connect()


@app.on_event("shutdown")
async def shutdown():
    await config.database.disconnect()


app.include_router(account_router.account_router)
app.include_router(validate_router.validate_router)

