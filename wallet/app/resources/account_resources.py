from fastapi import Depends
from dependencies import get_account_dal, get_rpc_dal


class AccountClass:
    """A class to interact with the accounts

    This is where we use the RPC and Account DALs to perform
    the logic to respond to our routers.  Creating, getting
    working with accounts is done in this class.

    Attributes
    ----------
    AccountDAL : the account data access layer
    RpcDAL : the rpc data access layer for the blockchain daemons

    Methods
    -------
    get_account_balance()
        Get account balance from the RPC endpoint with dals.rpc_dal

    get_account_address()
        Get account address from the DB with dals.account_dal

    create_new_account()
        Create a new account in the RPC and store it in the DB

    move_funds()
        Move a blance between one account to another, on the same coin

    send_to_address()
        Send funds to an address on the blockchain. This is a withdrawl.
    """

    def __init__(
        self, AccountDAL=Depends(get_account_dal), RpcDAL=Depends(get_rpc_dal)
    ):
        """
        Parameters
        ----------
        AccountDAL : class
            The account data access layer
        RpcDAL : class
            the RPC data access layer
        """
        self.AccountDAL = AccountDAL
        self.RpcDAL = RpcDAL

    async def get_account_balance(self, coin_id: int, account_label: str):
        """Get account balance from the RPC endpoint with dals.rpc_dal

        Using coin_id query the RPC daemon via the RPC DAL to get
        an account balance for an account label.  This is the only
        location for the balance, to have a single source of truth.

        Parameters
        ----------
        coin_id : int
            The id of the blockchain coin on the coinList API
        account_label : str
            The account identifier used between the blockchain daemon
            and the database
        """
        response = await self.RpcDAL.get_account_balance(coin_id, account_label)
        return response

    async def get_account_address(self, coin_id: int, account_label: str):
        """Get account address from the DB with dals.account_dal

        Lookup the account address in the database, to ensure we
        fetch the most recent and current address used across the
        platform.

        Parameters
        ----------
        coin_id : int
            The id of the blockchain coin on the coinList API
        account_label : str
            The account identifier used between the blockchain daemon
            and the database
        """

        response = await self.AccountDAL.get_account_address(coin_id, account_label)
        if response is None:
            row = "No Account Found"
        return response

    async def create_new_account(self, coin_id: int, account_label: str):
        """Create a new account in the RPC and store it in the DB

        We create a new account in the coin daemon via the RPC DAL,
        the blockchain address returned is then stored in the database.

        An account_label can have multiple addressses.

        Currently this will create a new blockchain address, even if
        one exists for the given account_label. This is intentional to
        allow account_labels to have multiple deposit addresses.

        Parameters
        ----------
        coin_id : int
            The id of the blockchain coin on the coinList API
        account_label : str
            The account identifier used between the blockchain daemon
            and the database
        """

        response = await self.RpcDAL.create_new_account(coin_id, account_label)
        await self.AccountDAL.store_new_account(coin_id, account_label, response)
        return response

    async def move_funds(
        self,
        coin_id: int,
        account_label_from: str,
        account_label_to: str,
        quantity: float,
    ):
        """Move a balance between one account to another, on the same coin.

        If two accounts exist on the same coin, funds can be moved
        from one account to another.  Checks are made to ensure required
        balance and both accounts exist.
        Parameters
        ----------
        coin_id : int
            The id of the blockchain coin on the coinList API
        account_label_from : str
            The source account identifier used in the transfer
        account_label_to : str
            The destination account identifier used in the transfer
        quantity : float
            How many coins to transfer between the accounts on a
            common blockchain daemon.
        """
        response = await self.RpcDAL.move_funds(coin_id, account_label_from, account_label_to, quantity)
        return response

    async def send_to_address(
        self,
        coin_id: int,
        account_label: str,
        address: str,
        quantity: float,
    ):
        """Send funds to an address on the blockhain.

        This is a send to an address on the blockchain. It serves
        as a withdrawl method. The address must be validated prior
        to this step.

        Parameters
        ----------
        coin_id : int
            The id of the blockchain coin on the coinList API
        account_label : str
            The source account identifier used in the transfer
        address : str
            The destination address on the blockchain
        quantity : float
            How many coins to send on the blockchain
        """

        balance = await self.get_account_balance(coin_id, account_label)
        if balance < quantity:
            return "Invalid Funds"

        account_address = await self.AccountDAL.get_account_address(
            coin_id, account_label
        )
        if account_address is None:
            return f"{account_label} is not a valid account_label"

        response = await self.RpcDAL.send_to_address(
            coin_id, account_label, address, quantity
        )
        return "Success"

