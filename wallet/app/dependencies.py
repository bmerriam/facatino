from db.config import database
from db.dals.account_dal import AccountDAL
from db.dals.rpc_dal import RpcDAL


async def get_account_dal():
    yield AccountDAL(database)
    """Access the basic db account functions

    This is the Account DAL used to acess the 
    database. It simply stores account addresses.

    Parameters
    ----------
    None

    """


async def get_rpc_dal(coin_id: int):
    yield RpcDAL(coin_id)
    """Access the RCP functions for a blockchain daemon

    This DAL grants access to balances, creating and querying accounts
    as well as moving funds. 
    
    Parameters
    ----------
    coin_id : int
        The id of the blockchain coin on the coinList API

    """
