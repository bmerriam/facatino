from fastapi import APIRouter, Depends

from resources.account_resources import AccountClass

from dependencies import get_account_dal, get_rpc_dal

validate_router = APIRouter()

@validate_router.get("/validate")
async def validate_address(
    coin_id: int, address: str, RpcDAL=Depends(get_rpc_dal)
):
    """Validate a blockchain address

    Check with a coin daemon via the RPC dal to
    confirm an address is valid on the blockchain.

    Parameters
    ----------
    coin_id : int
        The id of the blockchain coin on the coinList API
    address : str
        The address for the the blockchain
    """
    return await RpcDAL.validate_address(coin_id, address)