from fastapi import APIRouter, Depends
from fastapi.responses import RedirectResponse

from resources.account_resources import AccountClass

account_router = APIRouter()


@account_router.get("/")
async def index():
    """Return the user to the docs page on index

    There's nothing on the frontpage so we send users
    to the documentation by default.

    """
    return RedirectResponse("/docs")


@account_router.get("/account/balance")
async def get_account_balance(
    coin_id: int, account_label: str, Accounts=Depends(AccountClass)
):
    """Get the balance of an account

    Returns a float of the accounts balance in
    the blockchain daemon for the coin requested.
    If the account does not exist, it still returns
    a zero balance. The RPC DAL is used to get the
    balance.

    This is not an account validation endpoint. It
    it is only to confirm a deposit has been made.

    Parameters
    ----------
    coin_id : int
        The id of the blockchain coin on the coinList API
    account_label : str
        The account identifier used between the blockchain
        daemon and the database.
    Accounts : class
        The accounts resource thats uses the RPC DAL to get
        the balance.
    """
    return await Accounts.get_account_balance(coin_id, account_label)


@account_router.get("/account/address")
async def get_account_address(
    coin_id: int, account_label: str, Accounts=Depends(AccountClass)
):
    """Get the address for an account.

    Returns the blockchain address for an account
    for the coin daemon requested by coin_id. If
    an account does not exist, a null is returned.

    Parameters
    ----------
    coin_id : int
        The id of the blockchain coin on the coinList API
    account_label : str
        The account identifier used between the blockchain
        daemon and the database.
    Accounts : class
        The accounts resource thats uses the RPC DAL to get
        the balance.
    """
    print("Checking")
    return await Accounts.get_account_address(coin_id, account_label)


@account_router.post("/account/add")
async def create_new_account(
    coin_id: int, account_label: str, Accounts=Depends(AccountClass)
):
    """Create a new account

    This will generate a new blockchain address for a
    new account_label. It requests the address from
    the blockchain daemon for the coin_id requested
    then stores it in a DB before returning the address
    in a string format.

    Parameters
    ----------
    coin_id : int
        The id of the blockchain coin on the coinList API
    account_label : str
        The account identifier used between the blockchain
        daemon and the database.
    Accounts : class
        The accounts resource thats uses the RPC DAL to get
        the balance.
    """
    return await Accounts.create_new_account(coin_id, account_label)


@account_router.post("/account/move")
async def move_funds(
    coin_id: int,
    account_label_from: str,
    account_label_to: str,
    quantity: float,
    Accounts=Depends(AccountClass),
):
    """Move funds from one account to another.

    Move coins from one account to another, on the saame
    daemon.  This does not send coins across the blockchain.
    It is only used to delegate coins to an internal account
    on the blockchain daemon used and requested by coin_id.

    For example, A user can makes deposit to the address
    that we provide them. Then, a move can be performed
    to transfer the credits into another account.

    Parameters
    ----------
    coin_id : int
        The id of the blockchain coin on the coinList API
    account_label : str
        The account identifier used between the blockchain
        daemon and the database.
    Accounts : class
        The accounts resource thats uses the RPC DAL to get
        the balance.
    """
    return await Accounts.move_funds(
        coin_id, account_label_from, account_label_to, quantity
    )


@account_router.post("/account/send")
async def send_to_address(
    coin_id: int,
    account_label: str,
    address: str,
    quantity: float,
    Accounts=Depends(AccountClass),
):
    """Send funds to an address on the blockhain.

    This is a send to an address on the blockchain. It serves
    as a withdrawl method. The address must be validated prior
    to this step.

    Parameters
    ----------
    coin_id : int
        The id of the blockchain coin on the coinList API
    account_label : str
        The source account identifier used in the transfer
    address : str
        The destination address on the blockchain
    quantity : float
        How many coins to send on the blockchain
    """

    return await Accounts.send_to_address(
        coin_id, account_label, address, quantity
    )
