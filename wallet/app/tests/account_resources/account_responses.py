
class AccountTestClass:
    """ 
        A Testing class to configure return responses 
        with the requirement of an actual rpc daemon
    """
    async def get_account_balance(self, coin_id: int, account_label: str):
        """ 
            Return the balance for the requested coin_id and account_label
        """
        return ""

    async def get_account_address(self, coin_id: int, account_label: str):
        """
            Return the requested account address for the coin_id and account_label
        """
        return ""

    async def create_new_account(self, coin_id: int, account_label: str):
        """
            Return a new account object for creation
        """
        return "account_address"

    async def move_funds(self, coin_id: int, account_label_from: str, account_label_to: str, quantity: float):
        """
            Return a valid float to confirm the move worked.async
        """
        return {"coin_id": coin_id, "account_label_from": account_label_from, "account_label_to": account_label_to, "quantity": quantity}

    async def send_to_address(self, coin_id: int, account_label: str, address: str, quantity: float):
        """
            Return expected values from a send_to_address on the blockchain
        """
        return {"coin_id": coin_id, "account_label": account_label, "address": address, "quantity": quantity}
