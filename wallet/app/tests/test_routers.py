from fastapi.testclient import TestClient
"""
    This is the routers pytest file.

    The goal of these tests is to ensure the routes
    are setup correctly and response to expected and
    unexpected data correctly.
"""
from main import app

# Import our account class and then override
from resources.account_resources import AccountClass
from tests.account_resources import account_responses
app.dependency_overrides[AccountClass] = account_responses.AccountTestClass

client = TestClient(app)

def test_access_main():
    response = client.get("/")
    assert response.status_code == 200


def test_get_account_balance():
    response = client.get(
        "/account/balance?coin_id=1&account_label=test"
    )
    assert response.status_code == 200
    assert response.json() == ""


def test_get_account_address():
    response = client.get(
        "/account/address?coin_id=1&account_label=test"
    )
    assert response.status_code == 200
    assert response.json() == ""    

def test_post_account_add():
    response = client.post(
        "/account/add?coin_id=1&account_label=test"
    )
    assert response.status_code == 200
    assert response.json() == "account_address"    
    
def test_post_account_move():
    response = client.post(
        "/account/move?coin_id=1&account_label_from=from_label&account_label_to=to_label&quantity=1.1"
    )
    assert response.status_code == 200
    assert response.json() == {
        "coin_id": 1, 
        "account_label_from": "from_label", 
        "account_label_to": "to_label", 
        "quantity": 1.1
    }    
    
def test_send_to_address():
    response = client.post(
        "/account/send?coin_id=1&account_label=from_label&address=to_address&quantity=1.1"
    )
    assert response.status_code == 200
    assert response.json() == {
        "coin_id": 1, 
        "account_label": "from_label", 
        "address": "to_address", 
        "quantity": 1.1
    }    
